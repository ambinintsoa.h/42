/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antananari    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/06 10:28:27 by aherimam          #+#    #+#             */
/*   Updated: 2024/03/22 22:09:50 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

/**
 * Clean memory alloc and initialise the value on NULL
 * (cause this value is never re-initialise when a function
 * recall the variable and point in a memory after free)
 * a static variable
 * return NULL
*/
char	*freestatic(char **str)
{
	free(*str);
	*str = NULL;
	return (NULL);
}

/**
 * while buffer haven't \n
 * read and stock it on buffer
 * then join the value on stash
 * return stash the static variable
*/
char	*readline(int fd, char *stash)
{
	int		index;
	char	*buffer;

	index = 1;
	buffer = (char *)malloc((BUFFER_SIZE + 1) * sizeof(char));
	if (!buffer)
		return (freestatic(&stash));
	buffer[0] = '\0';
	while (index > 0 && !ft_strchr(buffer, '\n'))
	{
		index = read(fd, buffer, BUFFER_SIZE);
		if (index > 0)
		{
			buffer[index] = '\0';
			stash = ft_strjoin(stash, buffer);
		}
	}
	free(buffer);
	if (index == -1)
		return (freestatic(&stash));
	return (stash);
}

/**
 * take the value of 0 -> \n in stash
 * and return it on a new str
*/
char	*cleanline(char *stash)
{
	int		diff;
	char	*clean;

	diff = ft_strlen(stash) - ft_strlen(ft_strchr(stash, '\n'));
	clean = ft_substr(stash, 0, (diff + 1));
	if (!clean)
		return (NULL);
	return (clean);
}

/**
 * take the value of (\n + 1) -> \0
 * return it on new str
*/
char	*nextline(char *stash)
{
	size_t	start;
	char	*next;

	if (!ft_strchr(stash, '\n'))
	{
		next = NULL;
		return (freestatic(&stash));
	}
	else
		start = ft_strlen(stash) - ft_strlen(ft_strchr(stash, '\n')) + 1;
	if (!stash[start])
		return (freestatic(&stash));
	next = ft_substr(stash, start, ft_strlen(stash) - start);
	freestatic(&stash);
	if (!next)
		return (NULL);
	return (next);
}

/**
 * 1 - read line if stash is not NULLL or if it doesn't content \n
 * 2 - clean a stash to get 0 -> \n
 * 3 - remove the 0 -> \n in stash and stock the (\n + 1) -> \0 value
*/
char	*get_next_line(int fd)
{
	static char	*stash = {0};
	char		*line;

	if (fd < 0)
		return (NULL);
	if (!stash || (stash && !ft_strchr(stash, '\n')))
		stash = readline(fd, stash);
	if (!stash)
		return (NULL);
	line = cleanline(stash);
	if (!line)
		return (freestatic(&stash));
	stash = nextline(stash);
	return (line);
}
