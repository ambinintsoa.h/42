/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antananari    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/16 15:57:35 by aherimam          #+#    #+#             */
/*   Updated: 2024/03/22 21:57:37 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

/**
 * count the characters on str
 * return the count n
 */
size_t	ft_strlen(char *str)
{
	size_t	n;

	n = 0;
	if (!str)
		return (0);
	while (str[n] != '\0')
		n++;
	return (n);
}

/**
 * search the first occurence c in str
 * return str at the first occurence
 */
char	*ft_strchr(char *str, int c)
{
	size_t	len;

	len = ft_strlen(str);
	while (len > 0 && *str != (char)c)
	{
		str++;
		len--;
	}
	if (*str == (char)c)
		return ((char *)str);
	return (0);
}

/**
 * copy at str[start] in str with len size
 * return the result or NULL
 */
char	*ft_substr(char *s, unsigned int start, size_t len)
{
	char	*dest;
	size_t	i;

	if (start > ft_strlen(s))
		start = ft_strlen(s);
	if (len > ft_strlen(s) - start)
		len = ft_strlen(s) - start;
	dest = (char *)malloc(sizeof(char) * (len + 1));
	i = 0;
	if (!dest)
		return (0);
	if (start >= ft_strlen(s))
	{
		dest[0] = '\0';
		return (dest);
	}
	while (i < len && s[start] != '\0')
	{
		dest[i] = s[start];
		i++;
		start++;
	}
	dest[i] = '\0';
	return (dest);
}

/**
 * concatenate s1 and s2
 * return the new string
 */
char	*ft_strjoin(char *s1, char *s2)
{
	char	*r;
	size_t	i;
	size_t	j;

	if (!s1)
	{
		s1 = (char *)malloc(1 + sizeof(char));
		if (!s1)
			return (0);
		s1[0] = 0;
	}
	r = (char *)malloc((ft_strlen(s1) + ft_strlen(s2) + 1) * sizeof(char));
	if (!r)
		return (NULL);
	i = -1;
	while (s1[++i])
		r[i] = s1[i];
	j = -1;
	while (s2[++j])
		r[i + j] = s2[j];
	r[i + j] = '\0';
	free(s1);
	return (r);
}
