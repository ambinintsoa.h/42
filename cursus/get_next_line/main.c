/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antananari    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/13 21:06:48 by aherimam          #+#    #+#             */
/*   Updated: 2024/03/22 21:31:45 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int	main(void)
{
	int		fd;
	char	*line;

	fd = open("file.txt", O_RDONLY);
	if (!fd || fd < 0)
		return (0);
	while (fd >= 0)
	{
		line = get_next_line(fd);
		if (!line || line == NULL)
			return (0);
		else
			printf("%s", line);
		free (line);
	}
	return (0);
}
