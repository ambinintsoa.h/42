/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antanana      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/23 14:58:22 by aherimam          #+#    #+#             */
/*   Updated: 2024/02/23 14:58:24 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/**
 * change the value of the first part of str[n] in 0
 * return void (just remplace the str value of the temp variable)
 */
void	ft_bzero(void *str, size_t n)
{
	char	*temp;
	size_t	i;

	i = 0;
	temp = (char *)str;
	while (n > i)
	{
		temp[i] = 0;
		i++;
	}
	str = temp;
}
