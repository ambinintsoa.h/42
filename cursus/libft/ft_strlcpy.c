/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antanana      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/21 12:46:59 by aherimam          #+#    #+#             */
/*   Updated: 2024/02/25 10:48:57 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/**
 * copy src in dest
 * return the size of the result
 */
size_t	ft_strlcpy(char *dest, char *src, size_t n)
{
	size_t	size;
	size_t	i;

	i = 0;
	size = ft_strlen((char *)src);
	if (n != 0)
	{
		while (*(src + i) != '\0' && i < (n - 1))
		{
			*(dest + i) = *(src + i);
			i++;
		}
		*(dest + i) = '\0';
	}
	return (size);
}
