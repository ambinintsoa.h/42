/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/23 15:37:23 by aherimam          #+#    #+#             */
/*   Updated: 2024/02/25 14:57:29 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/**
 * compare the memory area of s1 an s2 at the first part of n
 * return 1 if s1>s2 or -1 if s1<s2 or 0 if s1=s2
 */
int	ft_memcmp(const void *p1, const void *p2, size_t n)
{
	char	*t1;
	char	*t2;

	t1 = (char *)p1;
	t2 = (char *)p2;
	while (n > 0)
	{
		if (*t1 != *t2)
		{
			if ((unsigned char)*t1 > (unsigned char)*t2)
				return (1);
			else if ((unsigned char)*t1 < (unsigned char)*t2)
				return (-1);
			else
				return (0);
		}
		t1++;
		t2++;
		n--;
	}
	return (0);
}
