/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/24 11:06:34 by aherimam          #+#    #+#             */
/*   Updated: 2024/03/01 12:17:56 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/**
 * malloc whith sizeof(n) the element size
 * and ft_bzero the p in element size after malloc
 * return p
 */
void	*ft_calloc(size_t element, size_t n)
{
	void	*p;

	p = (void *)malloc(n * element);
	if (!p)
		return (0);
	ft_memset(p, 0, n * element);
	return (p);
}
