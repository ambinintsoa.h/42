/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antanana      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/24 10:18:06 by aherimam          #+#    #+#             */
/*   Updated: 2024/02/24 10:18:09 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * change ascii to integer
 * return first sign * first integer
 */
int	ft_atoi(const char *str)
{
	int	space;
	int	sign;
	int	result;

	sign = 1;
	result = 0;
	space = 0;
	while ((*str >= 9 && *str <= 13) || *str == 32)
	{
		space++;
		str++;
	}
	if (*str == '+' || *str == '-')
	{
		if (*str == '-')
			sign *= -1;
		str++;
	}
	while (*str >= '0' && *str <= '9')
	{
		result = result * 10 + (*str - '0');
		str++;
	}
	return (sign * result);
}
