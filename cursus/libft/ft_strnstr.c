/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/24 07:18:03 by aherimam          #+#    #+#             */
/*   Updated: 2024/03/01 11:33:18 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/**
 * find word in str at the first part of str[n]
 * return str stated at the word or NULL
 */
char	*ft_strnstr(const char *str, const char *word, size_t n)
{
	size_t	i;
	char	*s;
	char	*w;

	s = (char *)str;
	w = (char *)word;
	if (!*w || w == s)
		return (s);
	while (*s != '\0' && n > 0)
	{
		i = 0;
		while (*(s + i) == *(w + i) && *(w + i) != '\0' && i < n)
		{
			if (*(w + i + 1) == '\0')
				return (s);
			i++;
		}
		n--;
		s++;
	}
	return (0);
}
