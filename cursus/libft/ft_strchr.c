/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/21 13:58:40 by aherimam          #+#    #+#             */
/*   Updated: 2024/03/01 10:51:02 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/**
 * search the first occurence c in str
 * return str at the first occurence
 */
char	*ft_strchr(const char *str, int c)
{
	size_t	len;

	len = ft_strlen(str);
	while (len > 0 && *str != (char)c)
	{
		str++;
		len--;
	}
	if (*str == (char)c)
		return ((char *)str);
	return (0);
}
