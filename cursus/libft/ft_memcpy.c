/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/19 13:53:08 by aherimam          #+#    #+#             */
/*   Updated: 2024/03/03 15:33:12 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/**
 * copy the content of first part of src[n] in destination
 * return the value of destination
 */
void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	int		i;
	char	*t_d;
	char	*t_s;

	i = 0;
	t_d = (char *)dest;
	t_s = (char *)src;
	if (t_d == t_s)
		return (dest);
	while (n > 0)
	{
		t_d[i] = t_s[i];
		n--;
		i++;
	}
	return (dest);
}
