/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antanana      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/19 14:44:10 by aherimam          #+#    #+#             */
/*   Updated: 2024/02/25 12:05:14 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/**
 * copy the content of first part of src[n] in destination,
 * reverse the copie methode if the dest content is > than src content
 * return the value of destination
 */
void	*ft_memmove(void *dest, const void *src, size_t n)
{
	int	i;
	int	reverse;

	if (dest == src)
		return (dest);
	if (dest > src)
	{
		reverse = (int)n - 1;
		while (reverse >= 0)
		{
			*(char *)(dest + reverse) = *(char *)(src + reverse);
			reverse--;
		}
	}
	else
	{
		i = 0;
		while (i < (int)n)
		{
			*(char *)(dest + i) = *(char *)(src + i);
			i++;
		}
	}
	return (dest);
}
