/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antanana      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/19 12:44:39 by aherimam          #+#    #+#             */
/*   Updated: 2024/02/21 09:46:58 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/**
 * set the value of the first part of src[n] with c
 * return the new value of str
 */
void	*ft_memset(void *str, int c, size_t n)
{
	int		i;
	char	*p;

	i = 0;
	p = (char *)str;
	while (n > 0)
	{
		p[i] = c;
		i++;
		n--;
	}
	return (str);
}
