/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antananari    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/23 15:19:06 by aherimam          #+#    #+#             */
/*   Updated: 2024/03/04 21:18:39 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/**
 * search the occurence of the first part of str[n]
 * return the occurence or NULL
 */
void	*ft_memchr(const void *str, int c, size_t n)
{
	char	*temp;
	char	l;

	temp = (char *)str;
	l = (char)c;
	while (n > 0)
	{
		if (*temp == l)
			return (temp);
		temp++;
		n--;
	}
	return (0);
}
