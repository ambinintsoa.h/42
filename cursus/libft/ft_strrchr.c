/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/21 14:51:35 by aherimam          #+#    #+#             */
/*   Updated: 2024/03/01 10:52:13 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/**
 * search the last occurence c in str
 * return str at the first occurence
 */
char	*ft_strrchr(const char *str, int c)
{
	char	*temp;
	int		len;

	temp = NULL;
	len = ft_strlen(str) + 1;
	while (len > 0)
	{
		if (*str == (char)c)
		{
			temp = (char *)str;
		}
		len--;
		str++;
	}
	return (temp);
}
