/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isascii.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antanana      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/19 11:05:06 by aherimam          #+#    #+#             */
/*   Updated: 2024/02/21 08:32:26 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * verify if it's a ascii 2^7
 * return true or false
 */
int	ft_isascii(int c)
{
	if (c >= 0 && c <= 127)
		return (1);
	else
		return (0);
}
