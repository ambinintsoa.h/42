/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_toupper.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/21 13:21:45 by aherimam          #+#    #+#             */
/*   Updated: 2024/03/02 10:44:36 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/**
 * transform lowercase in uppercase
 * return the character transformed
 */
int	ft_toupper(int c)
{
	if (ft_isalpha(c) == 1)
	{
		if (c >= 97 && c <= 122)
			return (c - 32);
		return (c);
	}
	else
		return (c);
}
