/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalpha.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antanana      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/19 09:22:23 by aherimam          #+#    #+#             */
/*   Updated: 2024/02/24 14:00:13 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * verify if it's an alphabetic character
 * return true or false
 */
int	ft_isalpha(int c)
{
	if ((c >= 65 && c <= 90) || (c >= 97 && c <= 122))
		return (1);
	else
		return (0);
}
