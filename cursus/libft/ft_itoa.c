/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antananari    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/03 11:08:18 by aherimam          #+#    #+#             */
/*   Updated: 2024/03/04 21:18:04 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static	int	ft_abs(int n)
{
	if (n < 0)
	{
		n *= -1;
		return (n);
	}
	return (n);
}

static	int	ft_len(int n)
{
	int	len;

	len = 0;
	if (n <= 0)
		len++;
	while (n != 0)
	{
		len++;
		n = n / 10;
	}
	return (len);
}

/**
 * Transform int in to char
*/
char	*ft_itoa(int n)
{
	char	*r;
	int		len;
	int		max_len;

	len = ft_len(n);
	max_len = len;
	r = (char *) malloc((len + 1) * sizeof(char));
	if (!r)
		return (0);
	if (n < 0)
		r[0] = '-';
	else if (n == 0)
		r[0] = '0';
	while (n != 0)
	{
		len--;
		r[len] = ft_abs(n % 10) + '0';
		n = n / 10;
	}
	r[max_len] = '\0';
	return (r);
}
