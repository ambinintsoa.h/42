/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tolower.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antanana      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/21 13:41:48 by aherimam          #+#    #+#             */
/*   Updated: 2024/02/21 13:41:50 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/**
 * transform uppercase in lowercase
 * return the character transformed
 */
int	ft_tolower(int c)
{
	if (ft_isalpha(c) == 1)
	{
		if (c >= 65 && c <= 90)
		{
			return (c + 32);
		}
		else
			return (c);
	}
	else
		return (c);
}
