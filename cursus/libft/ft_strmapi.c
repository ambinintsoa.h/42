/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/03 11:48:38 by aherimam          #+#    #+#             */
/*   Updated: 2024/03/03 15:34:00 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/**
 * copy every char in s in function f
 * to for example create a new string copy in f
*/
char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*r;
	int		len;
	int		i;

	i = 0;
	len = ft_strlen(s);
	r = (char *) malloc((len + 1) * sizeof(char));
	if (!s || !r || !f)
		return (0);
	while (s[i] != '\0')
	{
		r[i] = f (i, (char)s[i]);
		i++;
	}
	r[i] = '\0';
	return (r);
}
