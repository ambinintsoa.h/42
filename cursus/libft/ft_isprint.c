/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isprint.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antanana      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/19 12:18:27 by aherimam          #+#    #+#             */
/*   Updated: 2024/02/21 08:33:50 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * verify if it's printable
 * return true or false
 */
int	ft_isprint(int c)
{
	if (c >= 32 && c <= 126)
		return (1);
	else
		return (0);
}
