/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antanana      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/23 15:07:38 by aherimam          #+#    #+#             */
/*   Updated: 2024/02/23 15:07:40 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/**
 * find the bigger of str1 and str2 of the first par of n
 * return the difference
 */
int	ft_strncmp(const char *str1, const char *str2, size_t n)
{
	size_t	i;
	char	*s1;
	char	*s2;

	i = 1;
	s1 = (char *)str1;
	s2 = (char *)str2;
	if (n > 0)
	{
		while (*s1 == *s2 && *s1 != '\0' && i < n)
		{
			s1++;
			s2++;
			i++;
		}
		if (*s1 == *s2)
			return (0);
		else
			return ((unsigned char)*s1 - (unsigned char)*s2);
	}
	else
		return (0);
}
