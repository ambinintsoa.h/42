/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antanana      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/24 11:30:08 by aherimam          #+#    #+#             */
/*   Updated: 2024/02/24 11:30:10 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/**
 * duplicate the value of str using memory allocation
 * return the new variable
 */
char	*ft_strdup(const char *str)
{
	char	*s;
	char	*pc;
	char	*c;
	size_t	len;

	s = (char *)str;
	len = ft_strlen(s);
	pc = (char *)malloc(sizeof(char) * len + 1);
	c = pc;
	if (!pc)
		return (0);
	while (*s != '\0')
	{
		*pc = *s;
		pc++;
		s++;
	}
	*pc = '\0';
	return (c);
}
