/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/28 07:42:16 by aherimam          #+#    #+#             */
/*   Updated: 2024/03/03 15:34:41 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static	int	verif_start(const char *set, const char *s1)
{
	int	start;

	start = 0;
	while (ft_strchr(set, s1[start]))
		start++;
	if (start >= (int)ft_strlen(s1))
		return (0);
	return (start);
}

static	int	verif_end(const char *set, const char *s1, int start)
{
	int	end;

	end = ft_strlen(s1);
	while (ft_strrchr(set, s1[end - 1]) && start < end)
		end--;
	return (end);
}

/** 
 * find word (in variable set) at the start 
 * and at the end of str and remove it
 * return the result of str or NULL 
 */
char	*ft_strtrim(char const *s1, char const *set)
{
	char	*r;
	int		start;
	int		end;
	int		i;
	int		count;

	i = 0;
	start = verif_start(set, s1);
	end = verif_end(set, s1, start);
	count = end - start;
	r = (char *)malloc(sizeof(char) * count + 1);
	if (!r)
		return (0);
	while (i < count)
	{
		r[i] = s1[start];
		i++;
		start++;
	}
	r[i] = '\0';
	return (r);
}
