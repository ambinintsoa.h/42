/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antananari    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/01 13:35:41 by aherimam          #+#    #+#             */
/*   Updated: 2024/03/04 21:19:04 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/**
 * count word in str
 */
static int	ft_count_word(const char *str, char c)
{
	int	n;

	n = 0;
	if (!*str)
		return (0);
	while (*str != '\0')
	{
		while (*str == c)
			str++;
		if (*str != '\0')
			n++;
		while (*str && *str != c)
			str++;
	}
	return (n);
}

/**
 * count char in word
 */
static int	*ft_count_char(char const *str, char c, int i)
{
	int	*tab;
	int	index;
	int	count;

	index = i;
	count = 0;
	tab = (int *)malloc(2 * sizeof(int));
	if (!tab)
		return (0);
	while (str[index] == c && str[index] != '\0')
		index++;
	if (str[index] != c && str[index] != '\0')
	{
		while (str[index] != c && str[index] != '\0')
		{
			count++;
			index++;
		}
		tab[0] = index;
		tab[1] = count;
	}
	return (tab);
}

static void	**ft_free_all(char **tab)
{
	int	i;

	i = 0;
	while (tab[i])
	{
		free(tab[i]);
		i++;
	}
	free(tab);
	return (0);
}

static char	**ft_copy_tab(char **result, const char *s, char c, int col)
{
	int	i;
	int	index;
	int	*tab;

	i = 0;
	index = 0;
	while (i < col)
	{
		while (s[index] == c && s[index] != '\0')
			index++;
		tab = ft_count_char(s, c, index);
		result[i] = ft_substr(s, index, tab[1]);
		if (!result[i])
			ft_free_all(result);
		index = tab[0];
		free(tab);
		i++;
	}
	result[i] = 0;
	return (result);
}

/**
 * Separate words in string s and
 * place it in array 2 dimension
 */
char	**ft_split(char const *s, char c)
{
	char	**result;
	int		col;

	col = ft_count_word(s, c);
	result = (char **)malloc((col + 1) * sizeof(char *));
	if (!s || !result)
		return (0);
	result = ft_copy_tab(result, s, c, col);
	if (!result)
	{
		ft_free_all(result);
		return (0);
	}
	return (result);
}
