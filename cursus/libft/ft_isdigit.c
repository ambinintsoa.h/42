/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isdigit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aherimam <aherimam@student.42antanana      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/19 09:51:35 by aherimam          #+#    #+#             */
/*   Updated: 2024/02/19 15:18:23 by aherimam         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * verify if it's a number
 * return true or false
 */
int	ft_isdigit(int n)
{
	if (n >= 48 && n <= 57)
		return (1);
	else
		return (0);
}
